# terraform-pagerduty

[![pipeline status](https://gitlab.com/cwong47/terraform-pagerduty/badges/master/pipeline.svg)](https://gitlab.com/cwong47/terraform-pagerduty/commits/master)

This Terraform module creates the following resource
- Team
- User
- Schedule
- Escalation Policy
- Service
- Service Integration

# License and Authors

Authors: Calvin Wong
