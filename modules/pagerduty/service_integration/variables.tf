variable "name" {
  description = "Name of service integration. Make it meaningful"
}

variable "service" {}

variable "email" {
  default = ""
}
