########################
## schedule
########################

module "schedule_customer_success_01" {
  source = "../../modules/pagerduty/schedule"
  name   = "TF Test - ${var.team_customer_success} OnCall Level 1"

  users = [
    "${module.user_calvin_wong.id}",
    "PPM4NW9",
  ]
}

########################
## escalation policy
########################

/* module "escalation_policy_customer_success_01" { */
/*   source = "../../modules/pagerduty/escalation_policy" */
/*   name   = "TF Test - ${var.team_customer_success} Escalation Policy" */
/*   teams  = "${pagerduty_team.pd_team_techops.id}" */

/*   # harcoding :-\ */
/*   target_schedule_01 = "${module.schedule_customer_success_01.id}" */
/*   target_schedule_02 = "${module.schedule_customer_success_01.id}" */
/* } */

resource "pagerduty_escalation_policy" "escalation_policy_customer_success_01" {
  name      = "TF Test - ${var.team_customer_success} Escalation Policy"
  num_loops = 2
  teams     = ["${pagerduty_team.pd_team_techops.id}"]

  rule {
    escalation_delay_in_minutes = 15

    target {
      id = "${module.user_calvin_wong.id}"
    }
  }

  rule {
    escalation_delay_in_minutes = 15

    target {
      type = "schedule_reference"
      id   = "${module.schedule_customer_success_01.id}"
    }
  }

  rule {
    escalation_delay_in_minutes = 10

    target {
      type = "schedule_reference"
      id   = "${module.schedule_techops_sre_01.id}"
    }
  }
}

########################
## service
########################

module "service_customer_success_01" {
  source            = "../../modules/pagerduty/service"
  name              = "TF Test - Prod ${var.team_customer_success} - High Urgency"
  escalation_policy = "${pagerduty_escalation_policy.escalation_policy_customer_success_01.id}"
}

########################
## service integration
########################

module "service_integration_customer_success_01" {
  source  = "../../modules/pagerduty/service_integration"
  name    = "Zendesk"
  service = "${module.service_customer_success_01.id}"
  email   = "tf-test-cs-calvin-wong-zendesk@YOUR_COMPANY_NAME.pagerduty.com"
}

module "service_integration_customer_success_02" {
  source  = "../../modules/pagerduty/service_integration"
  name    = "Email"
  service = "${module.service_customer_success_01.id}"
  email   = "tf-test-cs-calvin-wong-email@YOUR_COMPANY_NAME.pagerduty.com"
}
