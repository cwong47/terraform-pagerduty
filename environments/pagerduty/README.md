# Setup

- Update the `pagerduty-secrets.tf` (this file is in `.gitignore`) file with the correct `pagerduty_token`.
- Run `terraform init` which will setup your environement with the tfstate on S3.
