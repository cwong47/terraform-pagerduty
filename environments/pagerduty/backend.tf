terraform {
  backend "s3" {
    bucket  = "YOUR_BUCKET_NAME"
    key     = "tfstate/pagerduty"
    region  = "us-east-1"
    encrypt = "true"
  }
}
