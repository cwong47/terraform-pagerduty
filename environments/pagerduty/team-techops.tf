########################
## schedule
########################

module "schedule_techops_sre_01" {
  source = "../../modules/pagerduty/schedule"
  name   = "TF Test - ${var.team_techops_sre} OnCall Level 1"

  users = [
    "${module.user_calvin_wong.id}",
    "PPM4NW9",
  ]
}

module "schedule_techops_sre_02" {
  source = "../../modules/pagerduty/schedule"
  name   = "TF Test - ${var.team_techops_sre} OnCall Level 2"

  users = [
    "PPM4NW9",
    "${module.user_calvin_wong.id}",
  ]
}

########################
## escalation policy
########################

resource "pagerduty_escalation_policy" "escalation_policy_techops_sre_01" {
  name      = "TF Test - ${var.team_techops_sre} Escalation Policy"
  num_loops = 2
  teams     = ["${pagerduty_team.pd_team_techops.id}"]

  rule {
    escalation_delay_in_minutes = 15

    target {
      type = "schedule_reference"
      id   = "${module.schedule_techops_sre_01.id}"
    }
  }

  rule {
    escalation_delay_in_minutes = 15

    target {
      type = "schedule_reference"
      id   = "${module.schedule_techops_sre_02.id}"
    }
  }
}

########################
## service
########################

module "service_techops_sre_01" {
  source            = "../../modules/pagerduty/service"
  name              = "TF Test - Prod ${var.team_techops_sre} - High Urgency"
  escalation_policy = "${pagerduty_escalation_policy.escalation_policy_techops_sre_01.id}"
}

########################
## service integration
########################

module "service_integration_techops_sre_01" {
  source  = "../../modules/pagerduty/service_integration"
  name    = "New Relic"
  service = "${module.service_techops_sre_01.id}"
}

module "service_integration_techops_sre_02" {
  source  = "../../modules/pagerduty/service_integration"
  name    = "Zendesk"
  service = "${module.service_techops_sre_01.id}"
  email   = "tf-test-calvin-wong-zendesk@YOUR_COMPANY_NAME.pagerduty.com"
}

module "service_integration_techops_sre_03" {
  source  = "../../modules/pagerduty/service_integration"
  name    = "Email"
  service = "${module.service_techops_sre_01.id}"
  email   = "tf-test-calvin-wong-email@YOUR_COMPANY_NAME.pagerduty.com"
}

module "service_integration_techops_sre_04" {
  source  = "../../modules/pagerduty/service_integration"
  name    = "CloudWatch"
  service = "${module.service_techops_sre_01.id}"
}

module "service_integration_techops_sre_05" {
  source  = "../../modules/pagerduty/service_integration"
  name    = "Slack"
  service = "${module.service_techops_sre_01.id}"
}
