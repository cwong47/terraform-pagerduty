########################
## variable
########################

variable "team_techops_sre" {
  default = "TechOps SRE"
}

variable "team_customer_success" {
  default = "Customer Success"
}

########################
## team
########################

resource "pagerduty_team" "pd_team_techops" {
  name = "TF Test - Team ${var.team_techops_sre}"
}

resource "pagerduty_team" "pd_team_cs" {
  name = "TF Test - Team ${var.team_customer_success}"
}
