########################
## user
########################

module "user_calvin_wong" {
  source    = "../../modules/pagerduty/user"
  name      = "TF Test - Calvin"
  email     = "YOUR_EMAIL_HERE"
  mobile    = "YOUR_MOBILE_HERE"
  job_title = "SRE"
  teams     = "${pagerduty_team.pd_team_techops.id}"
}
